<?php

chdir( dirname(__DIR__) );

define("SYS_PATH", "Core/");
define("APP_PATH", "app/");

require SYS_PATH."Router.php";
require APP_PATH."Http/routes.php";

$url = $_GET["url"];

try
{

    $action = Router::getAction($url);

    $controllerName = $action["controller"];
    $method = $action["method"];

    require APP_PATH."Controllers/".$controllerName.".php";
    spl_autoload_register(function ($clase) {

        require_once str_replace('\\','/',$clase.'.php');
    });

    $controller = new $controllerName();

    $controller->$method();

}
catch (Exception $e)
{
    echo $e->getMessage();
}
